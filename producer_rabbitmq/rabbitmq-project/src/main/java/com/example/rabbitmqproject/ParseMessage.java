package com.example.rabbitmqproject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ParseMessage {

    private File file = new File("/Users/hpm/Downloads/Activities.txt");

    public List<String> getReadLines() throws IOException {

        List<String> readLines = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            readLines.add(line);
        }
        return readLines;
    }

}

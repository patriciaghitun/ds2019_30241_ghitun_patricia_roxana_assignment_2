package com.example.rabbitmqproject;


import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Component
public class Send {

    private final static String QUEUE_NAME = "activity-queue";
    private static int messageQueueNr = 0;

    ParseMessage parseMessage = new ParseMessage();
    List<String> readLines = parseMessage.getReadLines();

    public Send() throws IOException {
    }

    @Scheduled(fixedDelay = 1000)
    public void sendActivityMessage() throws IOException, TimeoutException, JSONException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");

             try (Connection connection = factory.newConnection();
                     Channel channel = connection.createChannel()) {

                    //if(messageQueueNr < readLines.size()) {

                        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
                        String currentLine = readLines.get(messageQueueNr);
                        //parsarea liniei
                        String[] parts = currentLine.split("\\s\t"); // un spatiu si un tab
                        System.out.println("parts:" + currentLine);
                        //Integer patientId = Integer.parseInt(parts[0]);
                        String startString = parts[0];
                        String endString = parts[1];
                        String activityLabel = parts[2];

                        //crearea json-ului cu datele
                        JSONObject json = new JSONObject();
                        json.put("patientId", 1);
                        json.put("startDate", startString);
                        json.put("endDate", endString);
                        json.put("label", activityLabel);

                        channel.basicPublish("", QUEUE_NAME, null, json.toString().getBytes());
                        System.out.println(" [x] Sent '" + json.toString());
                        messageQueueNr++;

        }
    }
}



//
//    public static void main(String[] argv) throws Exception {
//
//        ConnectionFactory factory = new ConnectionFactory();
//        factory.setHost("localhost");
//
//        //Citirea din fisier:
//        File file = new File("/Users/hpm/Downloads/activity.txt");
//        BufferedReader br = new BufferedReader(new FileReader(file));
//
//
//        try (Connection connection = factory.newConnection();
//             Channel channel = connection.createChannel()) {
//             channel.queueDeclare(QUEUE_NAME, true, false, false, null);
//
//            //Aici trimit datele
//            int nrOfLines = 0;
//            String line;
//            while ((line = br.readLine()) != null) {
//                nrOfLines++;
//                //TimeUnit.SECONDS.sleep(1);
//
//                //parsarea liniei
//                String[] parts = line.split("\\s\t"); // un spatiu si un tab
//                System.out.println("parts:" + line);
//                Integer patientId = Integer.parseInt(parts[0]);
//                String startString = parts[1];
//                String endString = parts[2];
//                String activityLabel = parts[3];
//
//                //conversia string -> date
//                //Date startDate = new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(startString);
//                //Date endDate = new SimpleDateFormat("yyy-MM-dd HH:mm:ss").parse(endString);
//
//                //crearea json-ului cu datele
//                JSONObject json = new JSONObject();
//                json.put("patientId", patientId);
//                json.put("startDate", startString);
//                json.put("endDate", endString);
//                json.put("label", activityLabel);
//
//                channel.basicPublish("", QUEUE_NAME, null, json.toString().getBytes());
//                System.out.println(" [x] Sent '" + json.toString());
//            }
//        }
//    }
//
//
//
